
function grafica (){
    
    let lienzo=document.getElementById("miLienzo");
    let ctx=lienzo.getContext('2d');
    lienzo.width="1200";
    lienzo.height="300";
   
    let vo= document.getElementById("Vo");
        console.log('Vo= ',vo.value);
    let wo= document.getElementById("Wo");
        console.log('Wo=',wo.value,'°');
    woRadianes=wo.value*Math.PI/180;
        console.log('Wo(radianes)= ',woRadianes);
    
    let t=0,g=9.8,x=0,y=0;

    //Cañon
    ctx.beginPath();
    ctx.fillStyle='black'

    ctx.translate(15, 270);
    ctx.rotate(-woRadianes/2);
    ctx.translate(-15, -270);

    ctx.fillRect(15, 270, 90, 20);
    ctx.stroke();
    
    let xo=100, yo=275;

    for(let i=1;i<100;i++){         
        x= vo.value*(Math.cos(woRadianes))*t;  
        y= (vo.value*(Math.sin(woRadianes))*t)- (0.5*g*t*t); 
        ctx.beginPath();
        ctx.fillStyle="#b09";
        ctx.arc(x+xo, yo-y ,3 , 0, Math.PI*2, false);            
        ctx.stroke();
        ctx.fill();
        t=t+0.1;
    }
        




